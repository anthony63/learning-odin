package main

import "wgpu"
import "vendor:glfw"

State :: struct {
    surface: wgpu.Surface,
    device: wgpu.Device,
    queue: wgpu.Queue,
    width: int,
    height: int,
    render: proc(State),
    update: proc(State),
}

state_new :: proc(win: Window) -> State {
    width, height := glfw.GetWindowSize(win.window)

    instance := wgpu.CreateInstance(&wgpu.InstanceDescriptor {})
    
    
    return State {
        width = cast(int)width,
        height = cast(int)height,
        render = state_render,
        update = state_update,
    };
}

@(private="file")
state_render :: proc(State) {

}

@(private="file")
state_update :: proc(State) {
    
}