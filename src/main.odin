package main

import "core:fmt"
import "vendor:glfw"
import "vendor:vulkan"

main :: proc() {
    win := window_new(800, 600, "Hello world")
    win->run()
}