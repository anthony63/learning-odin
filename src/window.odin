package main

import "core:fmt"
import "vendor:glfw"

Window :: struct {
    window: glfw.WindowHandle,
    state: State,
    run: proc(^Window),
}

window_new :: proc(width, height: int, title: cstring) -> Window {
    if glfw.Init() != 1 {
        fmt.println("Failed to initialize glfw")
    }
    glfw.WindowHint(glfw.RESIZABLE, 0)
    return Window {
        window = glfw.CreateWindow(cast(i32)width, cast(i32)height, title, nil, nil),
        run = window_run,
    }
}

@(private="file")
window_run :: proc(win: ^Window) {
    win.state = state_new(win^)
    for !glfw.WindowShouldClose(win.window) {
        glfw.PollEvents()
    }
}