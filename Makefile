ODIN = odin build

SRC = src/
OUT = bin/learning-odin

$(OUT): $(SRC)/*.odin
	$(ODIN) $(SRC) -out:$(OUT)
run: $(OUT)
	./$(OUT)